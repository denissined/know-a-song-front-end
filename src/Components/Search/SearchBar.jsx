import React from "react";

const SearchBar = () => {
    return (
        <section className="search-bar">
            <input type="text" placeholder="Film Name"/>
        </section>
    );
};

export default SearchBar;