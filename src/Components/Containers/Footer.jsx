import React from "react";
import Copyright from "../Footer/Copyright";

const Footer = () => {
    return (
        <footer className="footer">
            <Copyright/>
        </footer>
    );
};

export default Footer;